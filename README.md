# html5videolightbox #

This very lightweight jQuery-Plugin allows you to easily create lightboxes that show a single video when clicked on a button.

## Features ##

* **Responsive** The size of the lightbox automatically adapts to any screen!
* **Lightness** html5videolightbox is merely 3kB large!
* Many more to come!

## Setup ##

### Step 1 ###

Download all the files in the js-, img- and css-folder.

### Step 2 ###

In your .html file, include the following three files (one of which is jQuery):


```
#!html
<!-- html5videolightbox requires jQuery -->
<script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>

<!-- these two files contain the entire project -->
<script src="js/html5videolightbox.js"></script>
<link href="css/html5videolightbox.css" type="text/css" rel="stylesheet">

```

### Step 3 ###

Create the following HTML somewhere in your <body> to include a video:


```
#!html

<!-- html for one of our videos -->
<div class="videoLightbox" id="videoBigBuckBunny">

    <!-- gray overlay to fadeout the rest of the page while showing the lightbox -->
    <div class="overlay"></div>

    <!-- white container to contain the video -->
    <div class="container">

        <!-- one of two ways to close the lightbox with id #videoBigBuckBunny -->
        <div class="closeVideoButton" data-target="#videoBigBuckBunny"></div>

        <!-- a title for our video (optionable) -->
        <h3 class="videoTitle">Video: Big Buck Bunny Baut Burgen Beim Bauern Ballerig</h3>

        <!-- wrapper for the video-element -->
        <div class="videoWrapper">

            <!-- width and height actually are irrelevant, but keep it just in case -->
            <video width="320" height="240" controls>

              <!-- add your video sources here -->
              <source src="http://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
              <source src="http://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
              Your browser does not support the video tag.
            </video>
        </div>
    </div>
</div>
<!-- /END OF OUR VIDEO -->
```

### Step 4 ###

Change the attributes according to your needs. Each div.videoLightbox should have a different id. The div.closeVideoButton must have its data-target attribute set to this id.

Also don't forget to include the right video files. You don't need to worry about the video having the corrent width- and height-attributes.

### Step 5 ###

Create a button or a div or an anchor-tag or what ever that you want to use to let the user open your lightbox. Simply add the class .openVideoButton and set the links data-target to the id of the .videoLightbox

### Step 6 ###

Your Lightbox should now be working. If you want to change any of the styling or javascript, feel free to do so. You can modify any of my code, but please always give credit.

Also, take a look at the index.html for an example use.

## ToDo for the future ##

* customisation
* less html
* ...

If you have any ideas or want to tell me your opinion about this lightbox, please write me.
