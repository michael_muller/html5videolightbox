$(document).ready(function() {

  /**
  * event listener for opening lightboxes
  */
  var openLightBox = function() {
    var button = $(this);
    //select the lightbox div
    var targetLightbox = $(button.data("target"));

    //fade in the lightbox
    targetLightbox.fadeIn(200);
  };

  /**
  * event listener for closing lightboxes via the button inside them
  */
  var closeLightBoxViaButton = function() {
    var button = $(this);
    //select the lightbox div
    var targetLightbox = $(button.data("target"));

    //pause the video
    targetLightbox.find("video").get(0).pause();
    //fade out the lightbox
    targetLightbox.fadeOut(200);
  }

  /**
  * event listener for closing lightboxes via the overlay outside them
  */
  var closeLightBoxViaOverlay = function() {
    var overlay = $(this);
    //select the lightbox div
    var targetLightbox = overlay.parent();

    //pause the video
    targetLightbox.find("video").get(0).pause();
    //fade out the lightbox
    targetLightbox.fadeOut(200);
  }

  //add event listeners to all elements
  $("body").delegate(".openVideoButton", "click", openLightBox);
  $("body").delegate(".closeVideoButton", "click", closeLightBoxViaButton);
  $("body").delegate(".overlay", "click", closeLightBoxViaOverlay);

});
